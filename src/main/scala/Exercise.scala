/** HY-562
    Assignment 2
    Fragkiadaki Vasileia
   **/

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

object Exercise1 {
  def main(args: Array[String]) {

    val conf = new SparkConf().setAppName("Exercise 1")
    val sc = new SparkContext(conf)
    val pagecounts = sc.textFile("C:/installations/spark-2.3.2-bin-hadoop2.7/bin/pagecounts-20160101-000000_parsed.out")
/*
    // 1. Retrieve the first 15 records and beautify
    getRecords(pagecounts, 15)

    // 2. Determine the number of records the dataset has in total
    getTotalRecords(pagecounts)

    // 3. Compute the min, max and average page size
    getMinPageSize(pagecounts)
    getMaxPageSize(pagecounts)
    getAveragePageSize(pagecounts)

    // 4. Determine the record with the largest page size, in case of multiple, list all of them
    getMaxPageSizeRec(pagecounts)

    // 5. Determine the record with the largest page size, pick the most popular
    getPopularMaxPSRec(pagecounts)

    // 6. Determine the record with the largest page title, list all of them
    getMaxPtitleRec(pagecounts)

    // 7. Use the results of question 3. and create a new RDD with the records that have
    // greater page size from the average
    getRecsGrTAverage(pagecounts)

    // 8. Compute the total number of pageviews per project
    getTotalPageviews(pagecounts)

    // 9. Report the 10 most popular pageviews of all projects
    getPopularPV(pagecounts)

    // 10. Determine the number of page titles that start with the article "The"
    // How many of those titles are not part of the English project
    getTitles(pagecounts)
    getTitlesNotEn(pagecounts)

    // 11. Determine the percentage of pages that have only received a single page view
    getSinglePVPages(pagecounts)

    // 12. Determine the number of unique terms appearing in the page titles
    getUniqueTerms(pagecounts)
*/
    // 13. Determine the most frequently occurring page title term in this dataset
    //using results of question 12.
    getMostFreqTitle(pagecounts)


    sc.stop

  }

    // Question 1.
    def getRecords(file:RDD[String], k:Int):Unit = {
      file.take(k).foreach(println)
    }

    // Question 2.
    def getTotalRecords(file:RDD[String]):Unit = {
      val totalRecords = file.count()
      println("Total Number of Records: " + totalRecords)
    }

    // Question 3.
    def getMinPageSize(file:RDD[String]):Unit = {
      val minPageSize = file.map(s => s.split(" ")).map(a => a(3).toLong).reduce((a,b)=> if (a<b) a else b)
      println("Min page size: " + minPageSize)
    }

    def getMaxPageSize(file:RDD[String]):Unit = {
      val maxPageSize = file.map(s => s.split(" ")).map(a => a(3).toLong).reduce((a,b)=> if (a>b) a else b)
      println("Max page size: " + maxPageSize)
    }

    def getAveragePageSize(file:RDD[String]):Unit = {
      val totalRecords = file.count()
      val totalPageSize = file.map(s => s.split(" ")).map(a => a(3).toLong).reduce(_+_)
      val averagePageSize:Long = (totalPageSize/(totalRecords))
      println("Average page size: " + averagePageSize)
    }

    // Question 4.
    def getMaxPageSizeRec(file:RDD[String]):Unit = {
      val max = file.map(s => s.split(" ")).map(a => a(3).toLong).reduce((a,b) => if (a>b) a else b)
      val records = file.map(s => s.split(" ")).map(a => (a(0), a(1), a(2), a(3).toLong)).filter(_._4 == max)
      println("Record with the largest page size:")
      records.collect().foreach(println)
    }

    // Question 5.
    def getPopularMaxPSRec(file:RDD[String]):Unit = {
      val max = file.map(s => s.split(" ")).map(a => a(3).toLong).reduce((a,b) => if (a>b) a else b)
      val record = file.map(s => s.split(" ")).map(a => (a(0), a(1), a(2).toLong, a(3).toLong)).filter(_._4 == max).sortBy(-_._3).take(1)
      println("Record with the largest page size, pick the most popular:")
      record.foreach(println)
    }

    // Question 6.
    def getMaxPtitleRec(file:RDD[String]):Unit = {
      val maxLength = file.map(s => s.split(" ")).map(a => (a(0), a(1), a(2), a(3))).sortBy(-_._2.length()).map(x => x._2.length).reduce((a,b) => if (a>b) a else b)
      val record = file.map(s => s.split(" ")).map(a => (a(0), a(1), a(2), a(3))).filter(_._2.length() == maxLength)
      println("Record with the largest page title:")
      record.collect().foreach(println)
    }

    // Question 7.
    def getRecsGrTAverage(file:RDD[String]):Unit = {
      val averagePageSize:Long = 132239

      val records = file.map(s => s.split(" ")).map(a => (a(0), a(1), a(2), a(3).toLong)).filter(_._4 > averagePageSize).take(15)
      println("Records that have greater page size from the average, 15 picked:")
      records.foreach(println)
    }

    // Question 8.
    def getTotalPageviews(file:RDD[String]):Unit = {
      val records = file.map(s => s.split(" ")).map(a => (a(0),a(2).toLong)).reduceByKey(_+_).take(15)
      println("Total number of pageviews per project, 15 picked:")
      records.foreach(println)
    }

    // Question 9.
    def getPopularPV(file:RDD[String]):Unit = {
      val records = file.map(s => s.split(" ")).map(a => (a(0),a(2).toLong)).reduceByKey(_+_).sortBy(-_._2)
      println("Ten most popular pageviews of all projects, sorted by the total number of hits:")
      records.take(15).foreach(println)
    }

    // Question 10.
    def getTitles(file:RDD[String]):Unit = {
      val numberOfTitles = file.map(s => s.split(" ")).map(a => a(1)).filter(_.length>3).map(a => if (a.substring(0,3).equalsIgnoreCase("The")) 1 else 0).reduce(_+_)
      println("Number of titles that start with the article \"The\": " + numberOfTitles)
    }

    def getTitlesNotEn(file:RDD[String]):Unit = {
      val titlesNotEn = file.map(s => s.split(" ")).map(a => (a(0), a(1))).filter(_._2.length>3).map(t => if (t._2.substring(0,3).equals("The")) (t._1, 1) else (t._1,0))
      .filter(_._2 == 1).filter(!_._1.equals("en")).count()
      println("Number of those titles that are not part of the English project: " + titlesNotEn)
    }

    // Question 11.
    def getSinglePVPages(file:RDD[String]):Unit = {
      val totalPages = file.count()
      val pagesSingleView = file.map(s => s.split(" ")).map(a => (a(2).toLong)).map(a=> if (a==1) 1 else 0).reduce(_+_)
      val percentage = Math.round ((pagesSingleView * 100) / totalPages)
      println("Percentage of pages that have only received a single page view: " + percentage + "%")
    }

    // Question 12.
    def getUniqueTerms(file:RDD[String]):Unit = {
      val uTerms = file.map(s => s.split(" ")).map(a => a(1).toLowerCase.replaceAll("[^a-z0-9_]", "")).flatMap(t => t.split("_")).map(t => t.trim()).
      map(word => (word,1)).reduceByKey(_+_).filter(_._2==1).count()
      println("The number of unique terms appearing in the page titles: " + uTerms)
    }

    // Question 13.
    def getMostFreqTitle(file:RDD[String]):Unit = {
      val mostFreqTerm = file.map(s => s.split(" ")).map(a => a(1).toLowerCase.replaceAll("[^a-z0-9_]", "")).flatMap(t => t.split("_")).map(t => t.trim()).
      map(word => (word,1)).reduceByKey(_+_).sortBy(-_._2).take(1).map(s=>s._1).apply(0)
      println("The most frequently occurring page title term: " + mostFreqTerm)
    }

}
